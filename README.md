# Articlomatic
C'est une application créée et maintenue par Hervé ALLESANT, ERUN à Marseille.

## A quoi ça sert ?
Cette application permet à un élève de créer une mise en page pour un article de journal, avec une image et du texte, en jouant sur la taille de la police, de l'image, et le nombre de colonnes.

## Licence GNU 3.0
Cf le document de licence pour voir les droits donnés par cette licence.