document.addEventListener("DOMContentLoaded", () => {
    // Références aux éléments du DOM
    const titleInput = document.getElementById("article-title");
    const titleSizeSlider = document.getElementById("title-size");
    const titleSizeValue = document.getElementById("title-size-value");
    const titlePositionSelect = document.getElementById("title-position");
    const contentInput = document.getElementById("article-content");
    const contentFontSizeSlider = document.getElementById("content-font-size");
    const contentFontSizeValue = document.getElementById("content-font-size-value");
    const imageInput = document.getElementById("article-image");
    const imageSizeSlider = document.getElementById("image-size");
    const imageSizeValue = document.getElementById("image-size-value");
    const imagePositionSelect = document.getElementById("image-position");
    const columnRadios = document.querySelectorAll("input[name='columns']");
    const articleSizeSlider = document.getElementById("article-size");
    const articleSizeValue = document.getElementById("article-size-value");
    const preview = document.getElementById("article-preview");
    const exportPngButton = document.getElementById("export-png");
    const exportPdfButton = document.getElementById("export-pdf");

    let imageDataUrl = "";

    // Fonction de mise à jour de l'aperçu de l'article
    function updatePreview() {
        console.log("Updating preview...");

        const titleText = titleInput.value;
        const titleSize = titleSizeSlider.value;
        const titlePosition = titlePositionSelect.value;
        const contentText = contentInput.value;
        const contentFontSize = contentFontSizeSlider.value;
        const numColumns = document.querySelector("input[name='columns']:checked").value;
        const imageSize = imageSizeSlider.value;
        const imagePosition = imagePositionSelect.value;
        const articleSize = articleSizeSlider.value;

        console.log("Title:", titleText);
        console.log("Title Size:", titleSize);
        console.log("Title Position:", titlePosition);
        console.log("Content:", contentText);
        console.log("Content Font Size:", contentFontSize);
        console.log("Number of Columns:", numColumns);
        console.log("Image Size:", imageSize);
        console.log("Image Position:", imagePosition);
        console.log("Article Size:", articleSize);

        const previewStyle = `
            column-count: ${numColumns};
            column-gap: 20px;
            text-align: justify;
            width: ${articleSize}cm;
            font-size: ${contentFontSize}px;
        `;

        // Construction du HTML pour l'aperçu
        let previewHtml = `
            <h1 class="article-title ${titlePosition}" style="font-size: ${titleSize}px;">${titleText}</h1>
            <div style="${previewStyle}">
        `;

        // Ajout de l'image dans le flux du texte
        if (imageDataUrl) {
            previewHtml += `
                <div class="image-container" style="width: ${imageSize}px; float: ${imagePosition}; margin: 10px;">
                    <img src="${imageDataUrl}" alt="Image" style="width: 100%; height: auto;">
                </div>`;
        }

        previewHtml += `${contentText}</div>`;

        preview.innerHTML = previewHtml;
    }

    // Mise à jour des valeurs des sliders
    function setupSlider(slider, valueDisplay) {
        slider.addEventListener("input", function () {
            valueDisplay.textContent = this.value;
            updatePreview();
        });
    }

    setupSlider(titleSizeSlider, titleSizeValue);
    setupSlider(contentFontSizeSlider, contentFontSizeValue);
    setupSlider(imageSizeSlider, imageSizeValue);
    setupSlider(articleSizeSlider, articleSizeValue);

    // Mise à jour du texte, des colonnes et du positionnement du titre
    titleInput.addEventListener("input", updatePreview);
    titlePositionSelect.addEventListener("change", updatePreview);
    contentInput.addEventListener("input", updatePreview);
    columnRadios.forEach(radio => {
        radio.addEventListener("change", updatePreview);
    });

    // Gestion de l'importation de l'image
    imageInput.addEventListener("change", function () {
        const file = this.files[0];
        const reader = new FileReader();

        reader.onload = function (e) {
            imageDataUrl = e.target.result;
            updatePreview();
        };

        if (file) {
            reader.readAsDataURL(file);
        }
    });

    imagePositionSelect.addEventListener("change", updatePreview);

    // Fonction pour exporter en PNG
    async function exportToPNG() {
        const exportButton = document.getElementById('export-png');
        exportButton.disabled = true;
        exportButton.textContent = 'Exportation en cours...';

        const content = document.getElementById('article-preview');
        
        try {
            const canvas = await html2canvas(content, {
                scale: 2,
                useCORS: true,
                logging: false,
                width: content.offsetWidth,
                height: content.offsetHeight
            });

            const imgData = canvas.toDataURL('image/png', 1.0);
            const link = document.createElement('a');
            link.href = imgData;
            link.download = 'article.png';
            link.click();
        } catch (error) {
            console.error('Erreur lors de l\'exportation du PNG:', error);
            alert('Une erreur est survenue lors de l\'exportation du PNG. Veuillez réessayer.');
        } finally {
            exportButton.disabled = false;
            exportButton.textContent = 'Exporter en PNG';
        }
    }

    // Fonction pour exporter en PDF
    async function exportToPDF() {
        const exportButton = document.getElementById('export-pdf');
        exportButton.disabled = true;
        exportButton.textContent = 'Exportation en cours...';

        const { jsPDF } = window.jspdf;
        const content = document.getElementById('article-preview');
        
        try {
            const canvas = await html2canvas(content, {
                scale: 2,
                useCORS: true,
                logging: false,
                width: content.offsetWidth,
                height: content.offsetHeight
            });

            const imgData = canvas.toDataURL('image/png', 1.0);
            const img = new Image();
            img.src = imgData;

            img.onload = function() {
                const imgWidth = img.width / 96 * 2.54; // Convert pixels to cm
                const imgHeight = img.height / 96 * 2.54; // Convert pixels to cm

                const doc = new jsPDF({
                    orientation: 'portrait',
                    unit: 'cm',
                    format: 'a4'
                });

                // Redimensionner l'image si elle dépasse les dimensions de la page A4
                const maxWidth = 21;
                const maxHeight = 29.7;
                let finalWidth = imgWidth;
                let finalHeight = imgHeight;

                if (imgWidth > maxWidth || imgHeight > maxHeight) {
                    const widthRatio = maxWidth / imgWidth;
                    const heightRatio = maxHeight / imgHeight;
                    const ratio = Math.min(widthRatio, heightRatio);
                    finalWidth = imgWidth * ratio;
                    finalHeight = imgHeight * ratio;
                }

                const xOffset = (21 - finalWidth) / 2; // Centrer l'image horizontalement
                const yOffset = (29.7 - finalHeight) / 2; // Centrer l'image verticalement

                doc.addImage(imgData, 'PNG', xOffset, yOffset, finalWidth, finalHeight);
                doc.save('article.pdf');
            };
        } catch (error) {
            console.error('Erreur lors de l\'exportation du PDF:', error);
            alert('Une erreur est survenue lors de l\'exportation du PDF. Veuillez réessayer.');
        } finally {
            exportButton.disabled = false;
            exportButton.textContent = 'Exporter en PDF';
        }
    }

    // Lier les boutons d'exportation aux fonctions correspondantes
    exportPngButton.addEventListener("click", exportToPNG);
    exportPdfButton.addEventListener("click", exportToPDF);

    // Initialisation de l'aperçu
    updatePreview();
});
